;;; read-eventX.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210519
;; Updated: 20210519
;; Version: 0.0
;; Keywords: keyboard input

;;; Commentary:

;; Started as copy of read-charX, but using `read-event' for reading.
;;

;;; Change Log:

;;; Code:


(require 'let1)


(defvar read-eventX/last1 nil
  "Last character read by `read-eventX'"
  )

(defvar read-eventX/repeat-seconds-to-wait
  2.0
  "Seconds to wait for repeated key press event."
  )

(defun read-eventX (&optional prompt inherit-input-method? seconds-to-wait)
  "Call function ‘read-event’ without echoing keystrokes."
  (save-excursion
    (let1 echo-keystrokes 0
      (when-let1
          event
          (read-event prompt inherit-input-method? seconds-to-wait)
        (setq read-eventX/last1 event)
        ))))


(defun read-eventX/repeated-command-event? (&optional unread-other-event? prompt inherit-input-method? seconds-to-wait)
  "Read an event and return it, if it matches last-command-event.
Otherwise return false
when UNREAD-OTHER-EVENT? true, push the event onto unread-command-events.

Avoid using a string value for UNREAD-OTHER-EVENT? as one might think it
was intended to be the PROMPT.
"
  (when (stringp unread-other-event?)
    (lwarn 'emacs :warning "unread-other-event? is a string, was it intended to be the prompt?")
    )
  (let1 event (read-eventX prompt inherit-input-method? seconds-to-wait)
    (if (eq event last-command-event)
        event
      (when unread-other-event? (push event unread-command-events))
      nil
      )))


(cl-defun read-eventX/push-back (&optional (event read-eventX/last1))
  "Push event EVENT onto `unread-command-events.

EVENT defaults to the charactacter most recently read by read-eventX"
  (push event unread-command-events)
  )


(defmacro read-eventX/do-repeat-while/last-command-event (&rest body)
  "Do BODY once, then if next key press is `last-command-event', do it again"
  (declare
   (debug body)
   )
  `(progn
     ,@body
     (let ((event last-command-event))
       (while
           (eq
            last-command-event
            (setq event (read-eventX nil read-eventX/repeat-seconds-to-wait))
            )
       ,@body
       )
       (push event unread-command-events)
       )))



(provide 'read-eventX)

;;; read-eventX.el ends here
