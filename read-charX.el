;;; read-charX.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210519
;; Updated: 20210519
;; Version: 0.0
;; Keywords: keyboard input

;;; Commentary:

;; Started as a simple wrapper around read-char-exclusive.
;;
;; Now provides some related convenience function and macros as well.

;;; Change Log:

;;; Code:


(require 'let1)


(defvar read-charX/last1 nil
  "Last character read by read-charX"
  )

(defvar read-charX/repeat-seconds-to-wait
  2.0
  "Seconds to wait for repeated key press event."
  )

(defun read-charX (&optional prompt inherit-input-method? seconds-to-wait)
  "Call function ‘read-char-exclusive’ without echoing keystrokes."
  (save-excursion
    (let (
          (echo-keystrokes 0)
          )
      (when-let1
          char-ev
          (read-char-exclusive prompt inherit-input-method? seconds-to-wait)
        (setq read-charX/last1 char-ev)
        ))))


(cl-defun read-charX/push-back (&optional (char-ev read-charX/last1))
  "Push event CHAR-EV onto `unread-command-events.

CHAR-EV defaults to the charactacter most recently read by read-charX"
  (push char-ev unread-command-events)
  )


(defmacro read-charX/do-repeat-while/last-command-event (&rest body)
  "Do BODY once, then if next key press is `last-command-event', do it again"
  (declare
   (debug body)
   )
  `(progn
     ,@body
     (let ((char-ev last-command-event))
       (while
           (eq
            last-command-event
            (setq char-ev (read-charX nil read-charX/repeat-seconds-to-wait))
            )
       ,@body
       )
       (push char-ev unread-command-events)
       )))



(provide 'read-charX)

;;; read-charX.el ends here
