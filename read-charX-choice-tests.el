;;; read-charX-choice-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231020
;; Updated: 20231020
;; Version: 0.0

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest read-charX-choice/spec ()
  "Try out read-charX-choice/default-keys."
  (let1
      read-charX-choice/default-keys
      [?a ?s ?d ?f ?g ?h ?j ?k ?l ?\;]
    (should-error
     (read-charX-choice/spec
      '(first-one  (?x . extra-x)  (y extra-y) "fred")
      ))
    (should
     (equal
      '((?a . first-one) (?x . extra-x) (?y . extra-y) (?s . "fred"))
      (read-charX-choice/spec
       '(first-one  (?x . extra-x)  (?y . extra-y)  "fred")
       )
      ))
    ))



;;; read-charX-choice-tests.el ends here
