;;; read-charX-choice.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231020
;; Updated: 20231020
;; Version: 0.0
;; Keywords: input by key

;;; Commentary:

;;; Change Log:

;;  20240109
;;  renamed from read-char-choice to read-charX-choice
;;  because I noticed then that a builtin function named
;;  read-char-choice appeared in subr.el (as of emacs24?)

;;; Code:


(require 's)
(require 'until)
(require 'alist-slash)
(require 'seq-slash)
(require 'read-charX)


(defvar read-charX-choice/default-keys
  [?a ?s ?d ?f ?g ?h ?j ?k ?l ?\;]
  "Default set of keys to be used with `read-charX-choice'."
  )

(defvar read-charX-choice/keys
  read-charX-choice/default-keys
  "Set of keys to be used with `read-charX-choice'"
  )



(defun read-charX-choice-with-keys/complete-spec (keys-to-choices)
  "Return read-charX-choice specification alist based on KEYS-TO-CHOICES.

KEYS-TO-CHOICES should be a loose alist with elements
of the form (KEY . CHOICE) or (KEY CHOICE)

KEY should be a character (e.g. number) representing a key event; or nil.

Redundant info in CHOICES-RAW
Currently, duplicate keys is an error, but duplicate values are allowed.

If KEYS-TO-CHOICES has nil key values, in the returned alist they are replaced
with characters taken from read-charX-choice/keys
"
  (cl-check-type keys-to-choices list)
  (let1 keys-used (alist/keys keys-to-choices)
    (when-let1  dups  (seq/dups (remove nil keys-used))
      (error "read-charX-choice spec has duplicate keys:  %S" dups)
      )
    (ifnot (member nil keys-used)
        keys-to-choices
      (let (
            (my/keys-to-choices (copy-alist keys-to-choices))
            (key-i  0)
            )
        ;; assign touches to elements with a null key
        (dolist (key-to-choice my/keys-to-choices)
          (cl-check-type key-to-choice list); list or dotted pair (key . choice)
          (unless (elt key-to-choice 0)
            (let1  new-key  (elt read-charX-choice/keys (post++ key-i))
              (do-repeat-while (assoc new-key keys-used)
                (when (length< read-charX-choice/keys key-i)
                  (error "read-charX-choice; number %d is more than the length of read-charX-choice/default-keys"
                         key-i
                         ))
                (setq new-key (elt read-charX-choice/keys (post++ key-i)))
                )
              (push new-key keys-used)
              (setcar key-to-choice new-key)
          )))
        my/keys-to-choices
        ))))


(defun read-charX-choice/key-string (key)
  "Return a string to represent the key of KV"
  (let1  key-string  (string key)
    (put-text-property 0 1 'face 'underline key-string)
    key-string
    ))

(defun read-charX-choice/default-namer (choice)
  "Return a string suitable to display CHOICE"
  (or (string? choice)
      (format "%S" choice)
      ))


(cl-defun read-charX-choice/guess-choice-name (thing)
  "Guess as a reasonable name to choose THING"
  (or (string? thing) (format "%S" thing))
  )

(cl-defun read-charX-choice (choices &key (prompt "") (choice-name-fun #'read-charX-choice/guess-choice-name))
  "Help user pick one element from a sequence of CHOICES.

Displays PROMPT and touche,choice-name pairs in the echo area
then waits for the user to press one those touches,
returning the corresponding selected choice.

Except, when CHOICES is singleton it is returned immediately.

The touches used are determined by `read-charX-choice/keys'.
If you want to stipulate which touches are used,
you can be temporarily bind `read-charX-choice/keys' to another sequence.
Or consider using `read-charX-choice/using-keys'

Currently, it is an error if there are more CHOICES that
the size of `read-charX-choice/keys'.
In the future a more elaborate display method may be implemented
to relieve this restriction.

When given, CHOICE-NAME-FUN should be a unary function taking elements
of CHOICES as its argument.
It should return a string to be displayed to the user.

See also `read-charX-choice/using-keys'.
"
  (cl-check-type choices sequence)
  (cl-assert (nempty? choices) t "read-charX-choice.  Error; choices empty.")
  (cl-assert (seq/len≦ choices read-charX-choice/default-keys) t
             "read-charX-choice.  Too many choices for current read-charX-choice/default-keys;\nSorry current implementation cannot handle this case."
             )
  (cdr
   (read-charX-choice/aux prompt read-charX-choice/keys choices choice-name-fun)
   ))

(cl-defun read-charX-choice* (choices &key (prompt "") (choice-name-fun #'read-charX-choice/guess-choice-name))
  "Like `read-charX-choice', but returns cons cell
(TOUCHE . CHOICE) where TOUCHE is the key press used to select
the choice CHOICE.

If there is only one CHOICE, the user is not actually prompted
to press a key, but instead (TOUCHE . CHOICE) of the only possible
choice is immediately returned."
  (cl-check-type choices sequence)
  (cl-assert (nempty? choices) t "read-charX-choice*.  Error; choices empty.")
  (cl-assert (seq/len≦ choices read-charX-choice/keys) t
             "read-charX-choice*.  Too many choices for current read-charX-choice/keys;\nSorry current implementation cannot handle this case."
             )
  (read-charX-choice/aux prompt read-charX-choice/keys choices choice-name-fun)
  )

(cl-defun read-charX-choice/using-keys (keys-to-choices &key (prompt ""))
  "Similar to `read-multiple-choice', but different in detail.

KEYS-TO-CHOICES should be a sequence with elements of the following form
  1. a cons cell (KEY . CHOICE), as in (?a . \"ardvaark\") or (?b . \"bee\"))
  2. a list of two elements (KEY CHOICE), as in (?a \"ardvaark\") or (?b \"bee\"))

where KEY should be a character (e.g. number) representing a key event.
If nil is given as a KEY for some choices, a key will be selected for
them according to read-charX-choice/keys'

Optional PROMPT is displayed before the choices.

Returns cons cells of the key pressed and its associated value.
For example, pressing the b key results in (?b . \"bee\") being returned.

Note that the entire list is returned
Optional PROMPT is displayed before the choices.

If CHOICES is a singleton, return that choice immediately;
without prompting or waiting for a key press.
"
  ;; TO CONSIDER:
  ;;  add parameter :choice-name-fun for more flexibility
  ;;  perhaps also accept [KEY CHOICE] format for KEYS-TO-CHOICES
(cdr (read-charX-choice/using-keys* keys-to-choices :prompt prompt))
)

(cl-defun read-charX-choice/using-keys* (keys-to-choices &key (prompt ""))
  "Like `read-charX-choice/using-keys', but returns cons cell
(TOUCHE . CHOICE) where TOUCHE is the key press used to select
the choice CHOICE.

If there is only one CHOICE, the user is not actually prompted
to press a key, but instead (TOUCHE . CHOICE) of the only possible
choice is immediately returned.
"
  (cl-check-type keys-to-choices list)
  (cl-assert keys-to-choices t "" "Error: read-charX-choice called with nil CHOICES list")
  (let1 keys-to-choices/complete (read-charX-choice-with-keys/complete-spec keys-to-choices)
    (read-charX-choice/aux
     prompt
     (mapcar #'car  keys-to-choices/complete)
     (mapcar #'secnd-dwim keys-to-choices/complete)
     #'read-charX-choice/default-namer
     )))


(cl-defun read-charX-choice/aux (prompt keys choices choice-name-fun)
  "Prompt user and return (TOUCHE . CHOICE) where
TOUCHE is the key pressed and CHOICE is the selected choice.

PROMPT is displayed first.

Displays PROMPT and touche,choice-name pairs in the echo area
then waits for the user to press one those touches,
returning the corresponding selected choice.

function CHOICE-NAME-FUN is called on choices in CHOICES
to obtain a string to show to the user.

Currently the ith key in KEYS select the ith choice in CHOICE.
In the future, keys may be recycled so that one can have more
CHOICES than the number of keys.

If CHOICES is a singleton, return that choice immediately with nil as its key
without prompting or waiting for a key press."
  ;; to consider:  give CHOICE-NAME-FUN (and possibly PROMPT) default values.
  (cl-assert (seq/len≦ choices keys) t
             "read-charX-choice/aux current cannot handle more choices than keys."
             )
  (if (len=1? choices)
      (cons (elt keys 0) (elt choices 0))
    (let1  cursor-type  '(bar . 0)
      (let1 key:choice-name/list (mapcar
                                  (lambda (i)
                                    (format "%s %s"
                                            (read-charX-choice/key-string (elt keys i))
                                            (funcall choice-name-fun (elt choices i))
                                            ))
                                  (number-sequence 0 (1- (min (length choices) (length keys))))
                                  )
        (let (
              (msg (format "%s%s"
                           prompt
                           (s-join   "  "   key:choice-name/list)
                           ))
              (echo-keystrokes 0)
              retval
              )
          (until-true retval
            (message msg); HACK. Redisplay every iteration, cuz pressing an invalid key seems to clear the echo area.
            (let* (
                   (touche (read-charX))
                   (key-idx (cl-position touche keys))
                   )
              (when (elt-safe choices key-idx)
                (setq retval (cons touche (elt choices key-idx)))
                )))))
      )))



(provide 'read-charX-choice)

;;; read-charX-choice.el ends here
